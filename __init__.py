"""This file contains all common utility function and imports"""
from flask_migrate import Migrate
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_login import LoginManager
from elasticsearch import Elasticsearch
from flask_babel import Babel , get_locale
from celery import Celery
import os

from .config import *

app = Flask(__name__)

app.config['SECRET_KEY'] = SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI


DB = SQLAlchemy(app)
migrate = Migrate(app, DB)

login = LoginManager(app)
login.login_view = 'login'
app.config['ELASTICSEARCH_URL'] = 'http://localhost:9200'
ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
babel = Babel(app)

SQLALCHEMY_TRACK_MODIFICATIONS = False
DEBUG = True

blacklist = set()
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access']
Elasticsearchdata= Elasticsearch([app.config['ELASTICSEARCH_URL']])

app.elasticsearch = Elasticsearchdata
jwts = JWTManager(app)


celery = Celery(
    app.name,
    backend=CELERY_BROKER_URL,
    broker=CELERY_RESULT_BACKEND)
print(app.name,'yjhgjhyvhjvfjyhfvjhyfcnhgcfnhg')
from flask_crud_apis_Fix import demo