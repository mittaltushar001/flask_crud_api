"""Api route file"""
import uuid
import datetime
import jwt
import requests
from flask import session
from requests.auth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, \
    check_password_hash
from sqlalchemy.exc import IntegrityError
from flask_jwt_extended import (jwt_required, create_access_token, get_raw_jwt)
from flask import request, jsonify, make_response, render_template, g, flash, redirect, url_for
from functools import wraps
from flask_login import logout_user, login_required, current_user, login_user
from flask_crud_apis_Fix import *
from .models import *
from .forms import RegistrationForm, LoginForm, AddQuestionForm, WriteAnswerForm, SearchForm, ChangePasswordForm
from .demo import check_data



@jwts.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """function to check if token is in checklist"""
    jti = decrypted_token['jti']
    return jti in blacklist


@app.before_request
def before_request():
    if current_user.is_authenticated:
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@app.route('/register', methods=['GET', 'POST'])
def register():
    """function use to register user"""
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        register_data = {"name": form.username.data, "password": form.password.data}
        response = requests.post('http://localhost:5000/api/register', json=register_data).json()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)


@app.route('/changepassword', methods=['GET', 'POST'])
def change_password():
    """function use to change password"""
    if current_user.is_authenticated:
        Bearer_token = "Bearer " + session['access_token']
        form = ChangePasswordForm()
        if form.validate_on_submit():
            update_question_json = {'oldpassword': form.oldpassword.data,
                                    'newpassword': form.newpassword.data}
            print(form.oldpassword.data,form.newpassword.data)
            response = requests.put('http://localhost:5000/api/changepassword',
                                    json=update_question_json,
                                    headers={"Authorization": Bearer_token,
                                             "token": session['token']}).json()
            flash('Your password changed successfully!')
            return redirect(url_for('home'))
        return render_template('changepassword.html', title='Changepassword', form=form)
    return redirect(url_for('home'))


@app.route('/', methods=['GET', 'POST'])
@app.route('/home', methods=['GET', 'POST'])
def home():
    """function use to display home page"""
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(name=form.username.data).first()
        if check_password_hash(user.password, form.password.data):
            response = requests.get('http://localhost:5000/api/login',
                                    auth=HTTPBasicAuth(user.name, form.password.data)).json()
            session['token'] = response['token']
            session['access_token'] = response['access_token']
            print(session['access_token'])
        else:
            flash('Invalid username or password')
            return redirect(url_for('home'))

        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('home.html', title='Sign In', form=form)


@app.route('/questions')
def index():
    """function to display list of questions"""
    if current_user.is_authenticated:
        if not session['access_token'] or session['token']:
           return redirect(url_for('home'))
        Bearer_token = "Bearer " + session['access_token']
        response = requests.get('http://localhost:5000/api/questions',
                            headers={"Authorization": Bearer_token,
                                     "token": session['token']}).json()
        user = User.query.filter_by(name=current_user.name)
        if len(response['questions']) > 0:
            return render_template('index.html', title='home', user=user,
                                   questions=response['questions'])
        else:
            return render_template('index.html', title='home', user=user)
    else:
        flash('Secure website only logging user can see questions')
        return redirect(url_for('home'))


@app.route('/questions/<question_id>/answers/')
def answer(question_id):
    """function to display list of answers"""
    if not session['access_token'] or session['token']:
        flash('Please login')
        return redirect(url_for('home'))
    Bearer_token = "Bearer " + session['access_token']
    response = requests.get('http://localhost:5000/api/questions/' + str(question_id) + '/answers',
                            headers={"Authorization": Bearer_token,
                                     "token": session['token']}).json()
    answers = AnswersDatas.query.filter_by(question_id=question_id)
    question = Questions.query.filter_by(id=question_id).first()
    if current_user.id == question.user_id:
        return render_template('answers.html', answers=answers,
                               questionid=question_id, question=question,
                               current_user=current_user, current_flag=current_user.id)

    return render_template('answers.html', answers=answers,
                           questionid=question_id, question=question)


@app.route('/user/<username>')
def user(username):
    """function to display current user details"""
    if not session['access_token'] or session['token']:
        flash('Please login')
        return redirect(url_for('home'))
    Bearer_token = "Bearer " + session['access_token']
    response = requests.get('http://localhost:5000/api/users/' + str(username),
                            headers={"Authorization": Bearer_token,
                                     "token": session['token']}).json()
    questions = Questions.query.filter_by(user_id=current_user.id).all()
    return render_template('user.html', username=response['user']['name'],
                           questions=questions, curent_user_name=current_user.name)


@app.route('/users')
def users():
    """function to display list of users """
    if not session['access_token'] or session['token']:
        flash('Please login')
        return redirect(url_for('home'))
    Bearer_token = "Bearer " + session['access_token']
    response = requests.get('http://localhost:5000/api/users',
                            headers={"Authorization": Bearer_token,
                                     "token": session['token']}).json()
    users = []
    for userinfo in response['users']:
        users = users + [{'name': userinfo['name']}]
    return render_template('users.html', users=users)


@app.route('/logout')
def logout():
    """This function logs out user"""
    if not session['access_token'] or session['token']:
        return redirect(url_for('home'))

    # Api token logout
    session.pop('access_token', None)
    session.pop('token', None)

    # UI interface logout
    logout_user()
    return redirect(url_for('home'))


@app.route('/newquestions', methods=['GET', 'POST'])
def add_question():
    """function to add questions and get questions"""
    if not session['access_token'] or session['token']:
        flash('Please login')
        return redirect(url_for('home'))

    form = AddQuestionForm()
    if form.validate_on_submit():
        Bearer_token = "Bearer " + session['access_token']
        update_question_json = {'text': form.question.data}
        response = requests.post('http://localhost:5000/api/questions',
                                 json=update_question_json,
                                 headers={"Authorization": Bearer_token,
                                          "token": session['token']}).json()
        question = Questions.query.filter_by(text=form.question.data).first()
        flash('Congratulations, you added a new question!')
        return redirect(url_for('answer', question_id=question.id))
    else:
        return render_template('addquestion.html', title='Question', form=form)


@app.route('/questions/<question_id>/answer/new', methods=['GET', 'POST'])
def addanswer(question_id):
    """function to add answer and get answer"""
    if not session['access_token'] or session['token']:
        flash('Please Register to write answer')
        return redirect(url_for('home'))
    if current_user.is_authenticated:
        form = WriteAnswerForm()
        if form.validate_on_submit():
            Bearer_token = "Bearer " + session['access_token']
            update_question_json = {'solution': form.answer.data}
            response = requests.post('http://localhost:5000/api/questions/' + str(question_id) + '/answers',
                                     json=update_question_json,
                                     headers={"Authorization": Bearer_token,
                                              "token": session['token']}).json()
            flash('Congratulations, you added a new answer!')
            return redirect(url_for('answer', question_id=question_id))
        else:
            return render_template('addanswer.html', title='Answer', form=form)
    else:
        flash('Please Register to write answer')
        return redirect(url_for('home'))


@app.route('/updatequestion/<questionid>', methods=['GET', 'POST'])
def updatequestion(questionid):
    """function to get and add a question"""
    if not session['access_token'] or session['token']:
        flash('Please Register to update question')
        return redirect(url_for('home'))
    question = Questions.query.get_or_404(questionid)
    if current_user.is_authenticated:
        Bearer_token = "Bearer " + session['access_token']
        if question.user_id == current_user.id:
            form = AddQuestionForm()
            if form.validate_on_submit():
                update_question_json = {'text': form.question.data}
                print(update_question_json)
                response = requests.put('http://localhost:5000/api/questions/' + str(questionid),
                                        json=update_question_json,
                                        headers={"Authorization": Bearer_token,
                                                 "token": session['token']}).json()
                flash('question updated')
                return redirect(url_for('answer', question_id=question.id))
            elif request.method == 'GET':
                form.question.data = question.text
                return render_template('updatequestion.html', title='Question', form=form)

        else:
            flash('You are not the author')
            return redirect(url_for('home'))
    else:
        flash('Please Register to update question')
        return redirect(url_for('home'))


@app.route('/updateanswer/<questionid>/<answerid>', methods=['GET', 'POST'])
def updateanswer(questionid, answerid):
    """function to get and update answers """
    if not session['access_token'] or session['token']:
        flash('Please Register to update answers')
        return redirect(url_for('home'))
    answer = AnswersDatas.query.filter_by(id=answerid).first()
    if current_user.is_authenticated:
        Bearer_token = "Bearer " + session['access_token']
        if answer.user_id == current_user.id:
            form = WriteAnswerForm()
            if form.validate_on_submit():
                print(form.answer.data)
                update_answers_json = {'solution': form.answer.data}
                response = requests.put(
                    'http://localhost:5000/api/questions/' + str(questionid) + '/answers/' + str(answerid),
                    json=update_answers_json, headers={"Authorization": Bearer_token, "token": session['token']}).json()
                flash('answer updated')
                return redirect(url_for('index'))
            elif request.method == 'GET':
                form.answer.data = answer.solution
                return render_template('updateanswer.html', title='Answer', form=form)
        else:
            flash('You are not the author')
            return redirect(url_for('home'))

    else:
        flash('Please Register to update answer')
        return redirect(url_for('home'))


@app.route('/deletequestion/<questionid>')
def deletequestion(questionid):
    """function to delete a question"""
    question = Questions.query.filter_by(id=questionid).first()
    if not session['access_token'] or session['token']:
        flash('Please Register to delete question')
        return redirect(url_for('home'))
    if current_user.is_authenticated:
        Bearer_token = "Bearer " + session['access_token']
        if current_user.id == question.user_id:
            response = requests.delete('http://localhost:5000/api/questions/' + str(questionid),
                                       headers={"Authorization": Bearer_token, "token": session['token']}).json()
            return redirect(url_for('index'))
        else:
            flash('You are not the author')
            return redirect(url_for('home'))
    else:
        flash('Please Register to delete question')
        return redirect(url_for('home'))


@app.route('/deleteanswer/<questionid>/<answerid>')
def deleteanswer(questionid, answerid):
    """function to delete a answer """
    if not session['access_token'] or session['token']:
        flash('Please Register to delete answers')
        return redirect(url_for('home'))
    if current_user.is_authenticated:
        Answer = AnswersDatas.query.filter_by(id=answerid).first()
        Bearer_token = "Bearer " + session['access_token']
        if current_user.id == Answer.user_id:
            response = requests.delete(
                'http://localhost:5000/api/questions/' + str(questionid) + '/answers/' + str(answerid),
                headers={"Authorization": Bearer_token, "token": session['token']}).json()
            return redirect(url_for('index'))
        else:
            flash('You are not the author')
            return redirect(url_for('home'))
    else:
        flash('Please Register to delete answer')
        return redirect(url_for('home'))


@app.route('/deleteuser/<userid>')
def deleteuser(userid):
    "function to delete user"
    if not session['access_token'] or session['token']:
        flash('Hacked and no access to deleted')
        return redirect(url_for('home'))
    if current_user.is_authenticated:
        Bearer_token = "Bearer " + session['access_token']
        response = requests.delete('http://localhost:5000/api/deleteuser/' + str(userid),
                                   headers={"Authorization": Bearer_token, "token": session['token']}).json()
        flash('user deleted')
        return redirect(url_for('logout'))
    else:
        flash('Hacked and no access to deleted')
        return redirect(url_for('home'))


###################################################### API ROUTES #########################################################################


@app.route('/api/register', methods=['POST'])
def signup():
    """This function to register username and password into database"""
    data = request.get_json()
    hashed_password = generate_password_hash(data['password'], method='sha256')

    new_user = User(public_id=str(uuid.uuid4()), name=data['name'],
                    password=hashed_password, admin=False)

    try:
        DB.session.add(new_user)
        DB.session.commit()
        return jsonify({'message': 'new user created'})
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error', "Status": 500})


@app.route('/api/login')
def login():
    """This function is use to get login detail and apply encoding password"""
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return make_response('Username or Password is compulsory', 401)
    session.pop('token', None)
    session.pop('access_token', None)
    user = User.query.filter_by(name=auth.username).first()
    if not user:
        return make_response('Invalid username or password', 401)

    print(user.password, auth.password)
    if check_password_hash(user.password, auth.password):
        token = jwt.encode(
            {'public_id': user.public_id,
             'exp': datetime.datetime.utcnow() + datetime.timedelta(days=999999999)},
            app.config['SECRET_KEY'])
        Decoded_token = token.decode('UTF-8')
        access_token = create_access_token(identity=user.name)
        responsedata = jsonify({'token': Decoded_token,
                                'access_token': access_token})

        return responsedata
    return make_response('Could not verify', 401)


def token_generator(function):
    """ This function is use to generate token"""

    @wraps(function)
    def decorated(*args, **kwargs):
        token = None
        if 'token' in request.headers:
            token = request.headers['token']

        if not token:
            return jsonify({'message': 'User token Missing!', "status": "401"})

        try:
            token_generate = jwt.decode(token, app.config['SECRET_KEY'])
            current_user_token = User.query.filter_by(public_id=
                                                      token_generate['public_id']).first()
        except:
            return jsonify({'message': 'Invalid user token!', "status": "404"})

        return function(current_user_token, *args, **kwargs)

    return decorated


@app.route('/api/logout', methods=['POST'])
@token_generator
@jwt_required
def log_out():
    """This function is use to logout of the access"""
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


@app.route('/api/questions', methods=['GET'])
@token_generator
@jwt_required
def get_all_questions(current_user_token):
    """This function is use to get all question by user"""
    current_user_token_questions = Questions.query.filter_by(user_id=current_user_token.id).all()
    user_questions = Questions.query.filter(Questions.user_id != current_user_token.id).all()

    if not user_questions and not current_user_token_questions:
        return jsonify({'message': 'No question found!', "status": 404})

    output = []

    for question in current_user_token_questions:
        all_questions = {'id': question.id, 'text': question.text, 'user': 'Current User'}
        output.append(all_questions)

    for question in user_questions:
        all_questions = {'id': question.id, 'text': question.text}
        output.append(all_questions)

    return jsonify({'questions': output, "status": 200})


@app.route('/api/questions', methods=['POST'])
@token_generator
@jwt_required
def create_question(current_user_token):
    """This function is use post question by user"""
    ask_question = request.get_json()

    new_question = Questions(text=ask_question['text'],
                             complete=True,
                             user_id=current_user_token.id)

    try:
        DB.session.add(new_question)
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': "Question created!", "status": 201})


@app.route('/api/changepassword', methods=['PUT'])
@token_generator
@jwt_required
def apichangepassword(current_user_token):
    """This function is use change logged user password"""
    password = request.get_json()
    user = User.query.filter_by(name=current_user_token.name).first()
    print(user.password)
    if check_password_hash(user.password, password['oldpassword']):
        user.password = generate_password_hash(password['newpassword'], method='sha256')


    try:
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': "Password Changed!", "status": 201})


@app.route('/api/questions/<question_id>', methods=['GET'])
@token_generator
@jwt_required
def get_one_question(current_user_token, question_id):
    """This function is use to get particular question by user"""

    current_user_token_question = Questions.query.filter_by(id=question_id).first()

    if not current_user_token_question:
        return jsonify({'message': 'No question found!', "status": 404})

    one_question = {'id': current_user_token_question.id, 'text': current_user_token_question.text}

    return jsonify({'questions': one_question, "status": 200})


@app.route('/api/questions/<question_id>', methods=['PUT'])
@token_generator
@jwt_required
def complete_question(current_user_token, question_id):
    """This function is use to change question of user by user"""
    update_question = request.get_json()

    question = Questions.query.filter_by(id=question_id,
                                         user_id=current_user_token.id).first()

    if not question:
        return jsonify({'message': 'No question found for you!', "status": 404})

    question.text = update_question['text']

    try:
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': 'Question has been changed!', "status": 200})


@app.route('/api/questions/<question_id>', methods=['DELETE'])
@token_generator
@jwt_required
def delete_question(current_user_token, question_id):
    """This function is use to delete question by user"""
    question = Questions.query.filter_by(id=question_id,
                                         user_id=current_user_token.id).first()

    if not question:
        return jsonify({'message': 'No question found!', "status": 404})

    AnswersDatas.query.filter_by(question_id=question_id,
                                 user_id=current_user_token.id).delete()

    Questions.query.filter_by(id=question_id,
                              user_id=current_user_token.id).delete()

    DB.session.commit()
    try:
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error', "Status": 500})
    return jsonify({'message': 'Question item deleted!', "status": 200})


@app.route('/api/questions/<question_id>/answers', methods=['GET'])
@token_generator
@jwt_required
def get_all_answers(current_user_token, question_id):
    """This function is use to get all answers by user"""

    current_user_token_answers = AnswersDatas.query.filter_by(question_id=question_id,
                                                              user_id=current_user_token.id).all()

    user_answers = AnswersDatas.query.filter(AnswersDatas.user_id != current_user_token.id,
                                             AnswersDatas.question_id == question_id).all()

    if not user_answers and not current_user_token_answers:
        return jsonify({'message': 'No answer found!', "status": 404})

    output = []

    for answer in current_user_token_answers:
        one_answer = {'id': answer.id, 'solution': answer.solution, 'user': 'Current User'}
        output.append(one_answer)

    for answer in user_answers:
        all_answers = {'id': answer.id, 'solution': answer.solution}
        output.append(all_answers)

    return jsonify({'answers': output, "status": 200})


@app.route('/api/questions/<question_id>/answers', methods=['POST'])
@token_generator
@jwt_required
def create_answer(current_user_token, question_id):
    """This function is use to create answer by user"""
    data = request.get_json()

    new_answer = AnswersDatas(solution=data['solution'], complete=True,
                              question_id=question_id,
                              user_id=current_user_token.id)
    try:
        DB.session.add(new_answer)
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': "Answer created!", "status": 201})


@app.route('/api/questions/<question_id>/answers/<answer_id>', methods=['GET'])
@token_generator
@jwt_required
def complete_answer(current_user_token, question_id, answer_id):
    """This function is use to change answer by user"""

    answer = AnswersDatas.query.filter_by(id=answer_id, question_id=question_id,
                                          user_id=current_user_token.id).first()

    if not answer:
        return jsonify({'message': 'No answer found!', "status": 404})

    output = {'id': answer.id, 'solution': answer.solution, "status": 200}

    return jsonify(output)


@app.route('/api/questions/<question_id>/answers/<answer_id>', methods=['PUT'])
@token_generator
@jwt_required
def change_answer(current_user_token, question_id, answer_id):
    """This function is use to change answer by user"""
    data = request.get_json()
    answer = AnswersDatas.query.filter_by(id=answer_id, question_id=question_id,
                                          user_id=current_user_token.id).first()

    if not answer:
        return jsonify({'message': 'No answer found!', "status": 404})

    answer.solution = data['solution']
    try:
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': 'answer has been changed!', "status": 200})


@app.route('/api/questions/<question_id>/answers/<answer_id>', methods=['DELETE'])
@token_generator
@jwt_required
def delete_answer(current_user_token, question_id, answer_id):
    """This function is use to delete answer by user"""
    answer = AnswersDatas.query.filter_by(id=answer_id, question_id=question_id,
                                          user_id=current_user_token.id).first()

    if not answer:
        return jsonify({'message': 'No answer found!', "status": 404})

    try:
        DB.session.delete(answer)
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': 'answer item deleted!', "status": 200})


@app.route('/api/deleteuser/<user_id>', methods=['DELETE'])
@token_generator
@jwt_required
def delete_user(current_user_token, user_id):
    '''Api delete user function to delete user'''
    AnswersDatas.query.filter_by(user_id=user_id).delete()
    Questions.query.filter_by(user_id=user_id).delete()
    User.query.filter_by(id=user_id).delete()
    try:
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})
    return jsonify({'message': 'user item deleted!', "status": 200})


@app.route('/api/updateuser/<userid>', methods=['PUT'])
@token_generator
@jwt_required
def change_userinfo(current_user_token, userid):
    """This Api function is use to change user id and password by user"""
    data = request.get_json()
    user = User.query.filter_by(id=current_user_token.id).first()

    if not user:
        return jsonify({'message': 'No user found!', "status": 404})

    user.name = data['name']
    user.password = data['password']
    try:
        DB.session.commit()
    except IntegrityError:
        DB.session.rollback()
        return jsonify({'message': 'Internal server error!', "status": 404})

    return jsonify({'message': 'user info has been changed!', "status": 200})


@app.route('/api/users/<username>', methods=['GET'])
@token_generator
@jwt_required
def get_one_user(current_user_token, username):
    '''This api function is use to output single user data'''
    user = User.query.filter_by(name=username).first()
    if not user:
        return jsonify({'message': 'No user found!', "status": 404})

    user_data = {'public_id': user.public_id, 'name': user.name, 'password': user.password, 'admin': user.admin}
    question_list = []
    questions = Questions.query.filter_by(user_id=user.id).all()
    for question in questions:
        question_list = question_list + [question.text]

    return jsonify({'user': user_data, "status": 200, "question": question_list})


@app.route('/api/users', methods=['GET'])
@token_generator
@jwt_required
def get_all_users(current_user_token):
    '''This api function is use to get all user data'''
    users = User.query.all()

    output = []

    for user in users:
        user_data = {'public_id': user.public_id, 'name': user.name, 'password': user.password, 'admin': user.admin}
        output.append(user_data)
    if len(output) == 0:
        return jsonify({'Message': 'No users yet', "status": 200})

    return jsonify({'users': output, "status": 200})


###################################################### SEARCH URI ###############################################

@app.route('/search')
@login_required
def search():
    '''This function searches for words inside databse in various models'''
    if not g.search_form.validate():
        return redirect(url_for('index'))
    page = request.args.get('page', 1, type=int)
    questions, total = Questions.search(g.search_form.q.data, page)
    answers, total_answers = AnswersDatas.search(g.search_form.q.data, page)
    user = User.query.filter_by(name=current_user.name)
    answer_all = answers.all()
    questions_answers_word = []
    for answer in answer_all:
        questionid = answer.question_id
        question_first = Questions.query.filter_by(id=questionid).first()
        answers_all = AnswersDatas.query.filter_by(id=answer.id, question_id=questionid).first()
        questions_answers_word = questions_answers_word + [(question_first.text, answers_all.solution, questionid)]
    if total or questions_answers_word:
        return render_template('index.html', title='home', user=user, questions=questions,
                               questions_answers_word=questions_answers_word)
    else:
        return render_template('index.html', title='home', user=user)


@app.route('/covidupdate', methods=['POST'])
def update_index():
    check_data.delay()
    return jsonify(Response="Data updated")


if __name__ == '__main__':
    app.run(debug=True)
