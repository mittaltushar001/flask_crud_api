This project demonstrates 

 - crud operation using rest api in flask framework.
 
 - Search using elastic search
 
 - Aynchronous operation using celery
 
 - Frontend interactions with the backend Api call uri's.

**About the project :-**
   
   - Databases --- question.db

                   Tables
                
                    ---- User
                 
                    ---- Question
                 
                    ---- Answers_datas
                 
    This project is an stackoverflow mini clone and has following technology implimentations which are :-     
      
                                                                                  - HTML,CSS and JAVASCRIPT for Frontend Interaction.
                                                                                  
                                                                                  - Elasticsearch feature for data searching.
                                                                                  
                                                                                  - Celery for asyncronous operations.
                                                                                  
                                                                                  - Flask as a backend language.
                                                                                  
                                                                                  
                                                                                  
   **Website Feature :-**
     
     --- CRUD operations on questions and answers
     
     --- Authorisation and authentication
     
     --- User profiles
     
     --- Search Feauture for searching questions and answers
     
     --- Asyncronous calls using celery
     
     
     


Api documentation is available online : https://documenter.getpostman.com/view/10731915/SzS8tR9y?version=latest


**Directory Structure :-**

                                ├── Databases
                                │   ├── question.db
                                |   |── databse_schema.png
                                │   
                                ├── config.py
                                |── models.py
                                ├── api.py
                                ├── README.md
                                └── requirements.txt
                                


**Dependencies :-**
                                
 use this command to install all dependencies:

    pip install -r requirements.txt


**Tools :-**

 Api checker tool like postman
 
                                

