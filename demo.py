""" Celery task to fetch covid data """
#from .Utils import *
from flask_crud_apis_Fix import *
from io import StringIO
from datetime import timedelta, date
import requests
import csv


@celery.task
def check_data():
    if Elasticsearchdata.ping():
        if Elasticsearchdata.indices.exists('covid'):
            Elasticsearchdata.indices.delete('covid')
            update_data()
        else:
            update_data()
    else:
        print('Connection error')
    return


def update_data():
    print('Loading data')
    Elasticsearchdata.indices.create('covid')
    current_data = date.today() - timedelta(days=1)
    print(current_data)
    current_data = current_data.strftime("%m-%d-%Y")
    COVID_DATA = requests.get(
        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data"
        "/csse_covid_19_daily_reports/" +
        current_data + ".csv").text
    print(COVID_DATA)
    COVID_DATA = StringIO(COVID_DATA)
    COVID_DATA = csv.DictReader(COVID_DATA)
    id = 0
    for data in COVID_DATA:
        id = id + 1
        Elasticsearchdata.index(index='covids', body=data, id=id)
    return
